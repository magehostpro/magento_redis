<?

function Redis_Connect()
{
    $sMagRoot = realpath( __DIR__ . '/../..' );
    $sXmlFile = $sMagRoot . '/app/etc/local.xml';

    if ( !is_readable( $sXmlFile ) )
    {
        die( $sXmlFile . ' does not exits or is not readable.' );
    }

    $oXml    = simplexml_load_file( $sXmlFile, 'SimpleXMLElement', LIBXML_NOCDATA );
    $sHost   = strval( $oXml->global->cache->backend_options->server );
    $sPort   = strval( $oXml->global->cache->backend_options->port );
    $sDb     = strval( $oXml->global->cache->backend_options->database );
    $sServer = '';

    if ( empty( $sHost ) )
    {
        die( 'Redis server is not found in local.xml' );
    }

    if ( '/' == substr($sHost,0,1) )
    {
        $sServer = sprintf( 'unix://%s', $sHost );
    }
    else
    { 
        if ( empty( $sPort ) )
        {
           die( 'Redis port is not found in local.xml' );
        }
        $sServer = sprintf( 'tcp://%s:%d', $sHost, $sPort );
    }
    if ( empty( $sDb ) )
    {
        die( 'Redis database number is not found in local.xml' );
    }

    require $sMagRoot . '/lib/Credis/Client.php';
    require $sMagRoot . '/lib/Zend/Cache/Backend/Interface.php';
    require $sMagRoot . '/lib/Zend/Cache/Backend/ExtendedInterface.php';
    require $sMagRoot . '/lib/Zend/Cache/Backend.php';
    require $sMagRoot . '/app/code/community/Cm/Cache/Backend/Redis.php';

    $oClient = new Credis_Client( $sServer );
    $oClient->select( $sDb );

    return $oClient;
}
